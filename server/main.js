const db = require("./db/mongo")
const express = require('express')
const { request } = require("express")
const app = express()
const port = 3001
const cors = require('cors');
db
const menu = require("./controllers/menu.js")
const category = require("./controllers/category.js")
const user = require("./controllers/user.js")
const promotion = require("./controllers/promotion.js")
const order = require("./controllers/order.js")

app.use(cors({
  origin: '*'
}));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})


app.use(express.json())
app.use("/menus", menu)
app.use("/categories", category)
app.use("/users", user)
app.use("/promotions", promotion)
app.use("/orders", order)