const mongoose = require("mongoose")

const Menu = mongoose.model('menus',new mongoose.Schema({
    name:{
        type:String
    },
    price:{
        type:Number
    },
    img:{
        type:String
    },
    category:{
        type: mongoose.Types.ObjectId,
        ref: "categories"
    },
    detail:{
        type:String
    }
}))

module.exports = Menu