const mongoose = require("mongoose")

const User = mongoose.model('users',new mongoose.Schema({
    name:{
        type:String
    },
    email:{
        type:String
    },
    password:{
        type:String
    },
    coin:{
        type:Number,
        default: () => 0
    },
}))

module.exports=User