const mongoose = require("mongoose")

const Order = mongoose.model('orders',new mongoose.Schema({
    user:{
        type: mongoose.Types.ObjectId,
        ref: "users"
    },
    menu:{
        type: mongoose.Types.ObjectId,
        ref: "menus"
    },
    status:{
        type: String,
        default: () => "Available"
    }
}))

module.exports = Order