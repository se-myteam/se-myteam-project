const mongoose = require("mongoose");

const Category = mongoose.model(
  "categories",
  new mongoose.Schema({
    categoryName: {
      type: String,
    }
  })
);

module.exports = Category;
