const mongoose = require("mongoose")

const Promotion = mongoose.model('promotions',new mongoose.Schema({
    image:{
        type:String
    },
    name:{
        type:String
    },
    discount:{
        type:Number
    },
    percentage:{
        type:Number
    },
    detail:{
        type:String
    }
}))

module.exports=Promotion