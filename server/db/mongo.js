const mongoose = require("mongoose")
let uri
// console.log(process.env.NODE_ENV)
if (process.env.NODE_ENV == "production") {
    uri = "mongodb://mongo:27017/CoffeePJ"
} else {
    uri = "mongodb://localhost:27017/CoffeePJ"
}

mongoose.connect(uri)

const db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("database ok"))

module.exports = db
