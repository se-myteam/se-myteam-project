const express = require('express')
const Promotion = require('../models/promotion')
const Category = require('../models/category')

const router=express.Router()
router.get('/' ,async (req,res) => {
    const promotions = await Promotion.find()
    res.send(promotions)
})

router.post('/', async function(req, res){
    const body = req.body;
    try {
        await Category.findOne({_id: body.category})
        await Promotion.create(body)
        res.send('post complete')
    } catch (error) {
        console.log(error)
        res.send('post incomplete')
    }
})

router.patch('/:id', async function(req, res){
    const id = req.params.id 
    const body = req.body
    try {
        await Category.findOne({_id: body.category})
        await Promotion.updateOne({
            _id: id 
        },
        {$set:body 
        })
        res.send('patch complete')
    } catch (error) {
        console.log(error)
        res.send('patch incomplete')
    }
})

router.delete('/:id', async function(req, res){
    const id = req.params.id
    await Promotion.deleteOne({
        _id: id        
    })
    res.send('delete complete')
})

router.get('/:id',async function(req, res){
    const id = req.params.id
    const promotion = await MenuPromotion.findOne({
        _id: id
    })
    res.send(promotion)    
})

module.exports=router

