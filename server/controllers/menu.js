const express = require('express')
const Menu = require('../models/menu')
const Category = require('../models/category')

const router=express.Router()
router.get('/' ,async (req,res) => {
    const menus = await Menu.find().populate("category")
    res.send(menus)
})

router.post('/', async function(req, res){
    const body = req.body;
    try {
        await Category.findOne({_id: body.category})
        await Menu.create(body)
        res.send('post complete')
    } catch (error) {
        console.log(error)
        res.send('post incomplete')
    }
})

router.patch('/:id', async function(req, res){
    const id = req.params.id 
    const body = req.body
    try {
        await Category.findOne({_id: body.category})
        await Menu.updateOne({
            _id: id 
        },
        {$set:body 
        })
        res.send('patch complete')
    } catch (error) {
        console.log(error)
        res.send('patch incomplete')
    }
})

router.delete('/:id', async function(req, res){
    const id = req.params.id
    await Menu.deleteOne({
        _id: id        
    })
    res.send('delete complete')
})

router.get('/:id',async function(req, res){
    const id = req.params.id
    const menu = await Menu.findOne({
        _id: id
    }).populate("category")
    res.send(menu)    
})

module.exports=router

