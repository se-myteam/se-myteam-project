const express = require('express')
const Category = require('../models/category')

const router=express.Router()
router.get('/' ,async (req,res) => {
    const categorys = await Category.find()
    res.send(categorys)
})

router.post('/', async function(req, res){
    const body = req.body;
    try {
        await Category.create(body)
        res.send('post complete')
    } catch (error) {
        console.log(error)
        res.send('post incomplete')
    }
})

router.patch('/:id', async function(req, res){
    const id = req.params.id 
    const body = req.body
    try {
        await Category.updateOne({
        _id: id 
        },
        {$set:body 
        })
        res.send('patch complete')
    } catch (error) {
        console.log(error)
        res.send('patch incomplete')
    }
})

router.delete('/:id', async function(req, res){
    const id = req.params.id
    await Category.deleteOne({
        _id: id        
    })
    res.send('delete complete')
})

router.get('/:id',async function(req, res){
    const id = req.params.id
    const category = await Category.findOne({
        _id: id
    })
    res.send(category)    
})

module.exports=router

