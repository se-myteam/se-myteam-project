const bcrypt = require('bcryptjs')
const express = require('express')
const { Error } = require('mongoose')
const User = require('../models/user')

const router = express.Router()
router.get('/', async (req, res) => {
    const users = await User.find()
    res.send(users)
})

router.post('/', async function (req, res) {
    const body = req.body;
    try {
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (body.email.match(mailformat)) {

            // เอาไว้ทำ hash ให้ password
            const hash = bcrypt.hashSync(body.password)
            await User.create({ name: body.name, email: body.email, password: hash })
            res.send('post complete')
        }else{
            throw new Error("Invalid Email");
        }
    } catch (error) {
        console.log(error)
        res.send('post incomplete')
    }
})

router.patch('/:id', async function (req, res) {
    const id = req.params.id
    const body = req.body
    try {
        await User.updateOne({
            _id: id
        },
            {
                $set: body
            })
        res.send('patch complete')
    } catch (error) {
        console.log(error)
        res.send('patch incomplete')
    }
})

router.delete('/:id', async function (req, res) {
    const id = req.params.id
    await User.deleteOne({
        _id: id
    })
    res.send('delete complete')
})

router.get('/:id', async function (req, res) {
    const id = req.params.id
    const category = await User.findOne({
        _id: id
    })
    res.send(category)
})

module.exports = router

