const express = require('express')
const Order = require('../models/order')
const User = require('../models/user')
const Menu = require('../models/menu')

const router=express.Router()
router.get('/' ,async (req,res) => {
    const orders = await Order.find().populate("user").populate("menu")
    res.send(orders)
})

router.post('/', async function(req, res){
    const body = req.body;
    try {
        await User.findOne({_id: body.user})
        await Menu.findOne({_id: body.menu})
        await Order.create(body)
        const user = await User.findOne({_id: body.user})
        user.coin += 20
        await user.save()
        res.send('post complete')
    } catch (error) {
        console.log(error)
        res.send('post incomplete')
    }
})

router.patch('/:id', async function(req, res){
    const id = req.params.id 
    const body = req.body
    try {
        await User.findOne({_id: body.user})
        await Menu.findOne({_id: body.menu})
        await Order.updateOne({
            _id: id 
        },
        {$set:body 
        })
        res.send('patch complete')
    } catch (error) {
        console.log(error)
        res.send('patch incomplete')
    }
})

router.delete('/:id', async function(req, res){
    const id = req.params.id
    await Order.deleteOne({
        _id: id        
    })
    res.send('delete complete')
})

router.get('/:id',async function(req, res){
    const id = req.params.id
    const order = await Order.findOne({
        _id: id
    }).populate("user").populate("menu")
    res.send(order)    
})

module.exports=router

