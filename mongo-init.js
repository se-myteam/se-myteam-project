db = db.getSiblingDB("admin");
db.createUser({
  user: "admin",
  pwd: "admin",
  roles: [
    { role: "userAdminAnyDatabase", db: "admin" },
    { role: "dbAdminAnyDatabase", db: "admin" },
    { role: "readWriteAnyDatabase", db: "admin" },
  ],
});
db = db.getSiblingDB("CoffeePJ");
db.createUser({
  user: "Pongpon",
  pwd: "1234",
  roles: [
    {
      role: "readWrite",
      db: "CoffeePJ",
    },
  ],
});

db.createCollection("menus");

db.menus.insertMany([
  {
    name: "coffee01",
    price: 50,
    category: "coffee",
    detail: "เย็น"
  },
  {
    name: "coffee02",
    price: 50,
    category: "coffee",
    detail: "ร้อน"
  },
  {
    name: "coffee03",
    price: 50,
    category: "Tea",
    detail: "เย็น"
  },
]);
