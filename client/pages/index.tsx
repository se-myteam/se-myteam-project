import Head from "next/head";
import Navbar from "../components/navbar/navBar";
import Promotion from "../components/promotion/promotion";
import OrderMenu from "../components/order/order";
import Underzone from "../components/underZone/underZone";
import classes from "./index.module.scss";
import React, { useEffect, useState } from "react";
import axios from "axios";

interface Icategories {
  _id?: string;
  categoryName?: string;
}

export default function Home() {
  const [categories, setCategories] = useState<Icategories[]>([]);
  useEffect(() => {
    axios.get("http://localhost:3001/categories").then((res) => {
      setCategories(res.data);
    });
  }, []);

  return (
    <div>
      <Head>
        <title>PJ Coffee</title>
      </Head>
      <Navbar />
      <Promotion />
      <div className={classes.header}>
        <span>Order</span>
      </div>
      <OrderMenu />
      <Underzone />
    </div>
  );
}
