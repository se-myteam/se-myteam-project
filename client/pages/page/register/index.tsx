import { Link } from "@material-ui/core";
import React from "react";
import Navbar from "../../../components/navbar/navBar";
import classes from "./index.module.scss";

export default function Register() {
  return (
    <div>
      <Navbar />
      <div className={classes.Main}>
        <div className={classes.bigBox}>
          <div className={classes.HeadText}>Welcome!</div>
          <div className={classes.SecondText}>
            sign up or log in to continue
          </div>
          <Link href="/" style={{ textDecoration: "none" }}>
            <div className={classes.facebook}>
                <img src="/facebook.svg" alt="facebook" />
                <span className={classes.text}>CONTINUE WITH FACEBOOK</span>
            </div>
          </Link>
          <div className={classes.or}>or</div>
          <Link href="/" style={{ textDecoration: "none" }}>
            <div className={classes.email}>
                <img src="/email.svg" alt="facebook" />
                <span className={classes.text}>CONTINUE WITH EMAIL</span>
            </div>
          </Link>
          <div>
              <input type="text" placeholder="YOURNAME" className={classes.input} />
          </div>
          <div>
              <input type="text" placeholder="EMAIL" className={classes.input} />
          </div>
          <div>
              <input type="text" placeholder="PASSWORD" className={classes.input} />
          </div>
          <div>
              <input type="text" placeholder="CONFIRM PASSWORD" className={classes.input} />
          </div>
          <div className={classes.registerText}>
              <span className={classes.text}>REGISTER</span>
          </div>
        </div>
      </div>
    </div>
  );
}
