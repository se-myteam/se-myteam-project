import Navbar from '../../components/navbar/navBar'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    test: {
        paddingTop: "70px",
    },
});

export default function TestNextPage() {
    const classes = useStyles();

    return (
        <div>
            <Navbar />
            <div  className={classes.test}>N1ext Page Success</div>
        </div>
    )
}