import Navbar from "../../../components/navbar/navBar";
import Radio from "../../../components/radiogroup/radiogroup";

import classes from "./id.module.scss";
import axios from "axios";
import { useRouter } from "next/dist/client/router";
import React, { useEffect, useState } from "react";
import { Link } from "@material-ui/core";

interface IMenu {
  _id?: string;
  detail?: string;
  img?: string;
  name?: string;
  price?: number;
}

export default function MenuById() {
  const router = useRouter();
  const [count, setCount] = useState<number>(1);

  const [menu, setMenu] = useState<IMenu>({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!router.query.id) return;

    axios.get(`http://localhost:3001/menus/${router.query.id}`).then((res) => {
      setMenu(res.data);
      setLoading(false);
    });
  }, [router.query]);
  function editMenu() {
    axios
      .patch(`http://localhost:3001/menus/${menu._id}`, menu)
      .then(() => window.alert("adadasd"));
  }
  function deleteMenu() {
    if (confirm()) {
      axios
        .delete(`http://localhost:3001/menus/${menu._id}`)
        .then(() => window.alert("delete success"));
    }
  }
  if (loading) {
    return <div>loading</div>;
  }

  return (
    <div>
      <Navbar />
      <div className={classes.Main}>
        <div className={classes.grid}>
          <div className={classes.BoxImg}>
            <img className={classes.img} src={menu.img} alt={menu.name} />
          </div>
          <div>
            <div>
              <div className={classes.edit}>Edit</div>
              <div className={classes.edit} onClick={deleteMenu}>
                DELETE
              </div>
              <input
                type="text"
                onChange={(e) => setMenu({ ...menu, name: e.target.value })}
                value={menu.name}
              />
              <div className={classes.edit} onClick={editMenu}>
                submit
              </div>
            </div>
            <div className={classes.PriceMenu}>ราคา {menu.price} บาท</div>
            {/* <div>{menu.detail}</div> */}
            <div>
              <Radio
                onChange={(value) => {
                  console.log(value);
                }}
              />
            </div>

            <div className={classes.buttonGroup}>
              <div
                className={classes.decreaseItem}
                onClick={() => {
                  if (count > 1) {
                    setCount(count - 1);
                  }
                }}
              >
                -
              </div>
              <input
                className={classes.input}
                type="text"
                value={count}
                onChange={(e) => {
                  setCount(Number(e.target.value));
                }}
              />
              <span
                className={classes.increaseItem}
                onClick={() => {
                  if (count < 99) {
                    setCount(count + 1);
                  }
                }}
              >
                +
              </span>
              <div>
                <Link style={{ textDecoration: "none" }} href="/">
                  <div className={classes.buying}>Buy</div>
                </Link>
              </div>
            </div>
            <div>
              <Link href="/" style={{ textDecoration: "none" }}>
                <div className={classes.buying2}>** buy with coin</div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
