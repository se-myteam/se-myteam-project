import Head from "next/head";
import Navbar from "../components/navbar/navBar";
import Promotion from "../components/promotion/promotion";
import OrderMenu from "../components/order/order";
import Underzone from "../components/underZone/underZone";
import classes from "./index.module.scss";
import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import axios from "axios";
import FormControl from "@mui/material/FormControl";
import Modal from "@mui/material/Modal";
import { MenuItem } from "@material-ui/core";

interface Icategories {
  _id?: string;
  categoryName?: string;
}

export default function Home() {
  const [orderOpen, setOrderOpen] = React.useState(false);
  const handleOrderOpen = () => setOrderOpen(true);
  const handleOrderClose = () => setOrderOpen(false);
  const [promotionOpen, setPromotionOpen] = React.useState(false);
  const handlePromotionOpen = () => setPromotionOpen(true);
  const handlePromotionClose = () => setPromotionOpen(false);

  const [Pname, setPName] = React.useState("");
  const [Pimg, setPimg] = React.useState("");
  const [Pdiscount, setPdiscount] = React.useState<number>();
  const [Ppercen, setPpercen] = React.useState<number>();
  const [Pdetail, setPdetail] = React.useState("");
  //  เอาไว้เก็บข้อมูล state ใน tag input **เหมือน flutter
  const [name, setName] = React.useState("");
  const [price, setPrice] = React.useState<number>();
  const [img, setImg] = React.useState("");
  const [detail, setDetail] = React.useState("");
  const [category, setCategory] = React.useState("");

  // เอาไว้เก็บข้อมูลที่มาจากapi เพื่อเอาไปสร้างdropdown ใน add order
  const [categories, setCategories] = useState<Icategories[]>([]);
  // เพราะในครั้งเเรกของการดึงข้อมูลใน api  มันจะยังโหลดข้อมูลมาไม่เสร็จเลยต้องสร้างสถานะนี้ขึ้นมาเพื่อ
  // ถ้ามันยังโหลดไม่เสร็จจะให้มันกลับไปทำงานใหม่จนกว่าจะโหลดข้อมูลมาครบ
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    axios.get("http://localhost:3001/categories").then((res) => {
      setCategories(res.data);
      setLoading(false);
    });
  }, []);

  const handleChange = (event: SelectChangeEvent) => {
    setCategory(event.target.value as string);
  };

  function createMenu() {
    axios
      .post("http://localhost:3001/menus", {
        name: name,
        price: price,
        detail: detail,
        img: img,
        category: category,
      })
      .then(() => {
        handleOrderClose();
        window.alert("สินค้าเพิ่มมาอีกหนึ่งชิ้นเเล้ว ยินดีด้วยน้าาาา (^_^)");
        location.reload();
      });
  }

  function createPromotion() {
    axios
      .post("http://localhost:3001/promotions", {
        name: Pname,
        Ppercen: Ppercen,
        detail: Pdetail,
        discount: Pdiscount,
        image: Pimg,
      })
      .then(() => {
        handlePromotionClose();
        window.alert(
          "มีโปรโมชั่นใหม่มาเเล้วว ไว้มาซื้อของกินกับเรานะ ヽ(；▽；)ノ"
        );
        location.reload();
      });
  }
  return (
    <div>
      <Head>
        <title>PJ Coffee</title>
      </Head>
      <Navbar />
      <Promotion />
      <div className={classes.header}>
        <span>Order</span>
        <div onClick={handleOrderOpen} className={classes.add}>
          Add Order
        </div>
        <Modal
          open={orderOpen}
          onClose={handleOrderClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box className={classes.modal}>
            <span>Add order</span>
            <div className={classes.inputGroup}>
              <input
                className={classes.inputt}
                type="text"
                placeholder="ชื่อสินค้า"
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <input
                className={classes.inputt}
                type="number"
                placeholder="ราคา"
                value={price}
                onChange={(e) => {
                  setPrice(Number(e.target.value));
                }}
              />
              <input
                className={classes.inputt}
                type="text"
                placeholder="รายละเอียด"
                value={detail}
                onChange={(e) => {
                  setDetail(e.target.value);
                }}
              />
              <input
                className={classes.inputt}
                type="text"
                placeholder="ลิ้งรูปภาพ"
                value={img}
                onChange={(e) => {
                  setImg(e.target.value);
                }}
              />
              <Box className={classes.inputt}>
                <FormControl className={classes.test} fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    category
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={category}
                    label="category"
                    onChange={handleChange}
                  >
                    {!loading &&
                      categories.map((e) => (
                        <MenuItem value={e._id} key={e._id}>
                          {e.categoryName}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Box>
              <div className={classes.submit} onClick={createMenu}>
                เพิ่มสินค้า
              </div>
            </div>
          </Box>
        </Modal>

        <div onClick={handlePromotionOpen} className={classes.add}>
          Add Promotion
        </div>
        <Modal
          open={promotionOpen}
          onClose={handlePromotionClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box className={classes.modal}>
            <span>Add promotion</span>
            <div className={classes.inputGroup}>
              <input
                className={classes.inputt}
                type="text"
                placeholder="ชื่อ"
                value={Pname}
                onChange={(e) => {
                  setPName(e.target.value);
                }}
              />
              <input
                className={classes.inputt}
                type="text"
                placeholder="รูป"
                value={Pimg}
                onChange={(e) => {
                  setPimg(e.target.value);
                }}
              />
              <input
                className={classes.inputt}
                type="number"
                placeholder="ส่วนลด"
                value={Pdiscount}
                onChange={(e) => {
                  setPdiscount(Number(e.target.value));
                }}
              />
              <input
                className={classes.inputt}
                type="number"
                placeholder="เปอร์เซ็น"
                value={Ppercen}
                onChange={(e) => {
                  setPpercen(Number(e.target.value));
                }}
              />
              <input
                className={classes.inputt}
                type="text"
                placeholder="รายละเอียด"
                value={Pdetail}
                onChange={(e) => {
                  setPdetail(e.target.value);
                }}
              />
              <div className={classes.submit} onClick={createPromotion}>
                เพิ่มสินค้า
              </div>
            </div>
          </Box>
        </Modal>
      </div>

      <OrderMenu />
      <Underzone />
    </div>
  );
}
