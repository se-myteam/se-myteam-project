import NotificationsIcon from '@material-ui/icons/Notifications';
import classes from './navbar.module.scss';
import { Divider } from '@material-ui/core';
import Link from 'next/link'



// export default function navBar(props: any) {
export default function navBar() {
    return (
        <div className={classes.Main}>
            <div className={classes.logo}>
                <div>
                    <img src="/LOGO.svg" alt="Logo" className={classes.logoImg} />
                </div>
                <div className={classes.logoText}>Cafe Coffee</div>
            </div>
            <div className={classes.latestLogo}>
                <Link href="https://www.w3schools.com/howto/howto_css_fixed_menu.asp">
                    <div className={classes.iconNav}>
                        <img src="/Profile.svg" alt="Profile" className={classes.icon} />
                    </div>
                </Link>
                <Divider orientation="vertical" className={classes.middleDivider} />
                <Link href="https://material-ui.com/components/material-icons/">
                    <div className={classes.iconNav}>
                        <img src="/Market.svg" alt="Market" className={classes.icon} />
                    </div>
                </Link>
                {/* <Divider orientation="vertical" className={classes.middleDivider} /> */}
                {/* <div className={classes.iconNav}>  test</div> */}
            </div>
            {/* {props.children} */}
        </div>
    )
}