import React from "react";
import Carousel from "react-material-ui-carousel";
import classes from "./promotion.module.scss";
import axios from "axios";
import { useEffect, useState } from "react";

interface IPromotion {
  image?: string;
  name?: string;
  discount?: number;
  percentage?: number;
  detail?: string;
}

export default function Promoiton() {
  const [promotion, setPromotion] = useState<IPromotion[]>([]);
  useEffect(() => {
    axios.get("http://localhost:3001/promotions").then((res) => {
      setPromotion(res.data);
    });
  }, []);

  return (
    <div>
      <Carousel>
        {promotion.map((e) => (
          <div className={classes.Main} key={e.name}>
            <div className={classes.test}>
              <img src={e.image} alt={e.name} />
            </div>
          </div>
        ))}
      </Carousel>
    </div>
  );
}
