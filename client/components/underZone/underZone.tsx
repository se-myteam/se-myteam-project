import { Link } from "@material-ui/core"
import React from "react"
import classes from "./underZone.module.scss"

export default function Underzone(){
    return (
        <div className={classes.grid}>
        <div className={classes.box1}>
          <span className={classes.headTxt}>ข้อมูลส่วนตัว</span>
          <Link href="https://www.w3schools.com/howto/howto_js_navbar_sticky.asp">
            <span className={classes.txt}> เหรียญ coin</span>
          </Link>
          <Link href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Realizing_common_layouts_using_CSS_Grid_Layout">
            <span className={classes.txt}>Promotion</span>
          </Link>
        </div>
        <div className={classes.box1}>
          <span className={classes.headTxt}>เมนูอาหาร</span>
          <Link href="https://www.w3schools.com/howto/howto_js_navbar_sticky.asp">
            <span className={classes.txt}>เค้ก</span>
          </Link>
          <Link href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Realizing_common_layouts_using_CSS_Grid_Layout">
            <span className={classes.txt}>ไอศกรีม</span>
          </Link>
          <Link href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Realizing_common_layouts_using_CSS_Grid_Layout">
            <span className={classes.txt}>กาแฟ</span>
          </Link>
        </div>
        <div className={classes.box1}>
          <span className={classes.headTxt}>ตะกร้า</span>
          <Link href="https://www.w3schools.com/howto/howto_js_navbar_sticky.asp">
            <span className={classes.txt}>ตระกร้าของฉัน</span>
          </Link>
          <Link href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Realizing_common_layouts_using_CSS_Grid_Layout">
            <span className={classes.txt}>สถานะการสั่งซื้อ</span>
          </Link>
          <Link href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Realizing_common_layouts_using_CSS_Grid_Layout">
            <span className={classes.txt}>สินค้าทั้งหมด</span>
          </Link>
        </div>
      </div>
    )
}