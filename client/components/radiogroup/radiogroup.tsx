import { useState } from "react";
import classes from "./radiogroup.module.scss";
interface Option {
  value: string;
}
interface Props {
  onChange: (value: Option) => any;
}
export default function Radiogroup(props: Props) {
  const [options, setOptions] = useState<Option[]>([
    { value: "Home" },
    { value: "Cafe" },
  ]);
  const [select, setSelect] = useState<number>(0);
  return (
    <div>
      {options.map((e, index) => (
        <div
          className={classes.Main}
          key={index}
          onClick={() => {
            setSelect(index);
            props.onChange(options[index])
          }}
        >
          {select == index ? <img src="/on.svg" className={classes.radioSelect} /> : <img src="/off.svg" className={classes.radioSelect} />}
          <div>{e.value}</div>
        </div>
      ))}
    </div>
  );
}
