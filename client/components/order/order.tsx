import { Link } from "@material-ui/core";
import classes from "./order.module.scss";
import axios from "axios";
import { useEffect, useState } from "react";

interface IMenu {
  _id?:string;
  detail?: string;
  img?: string;
  name?: string;
  price?: number;
}

export default function Order() {
  const [menu, setMenu] = useState<IMenu[]>([]);
  useEffect(() => {
    axios.get("http://localhost:3001/menus").then((res) => {
      setMenu(res.data);
    });
  }, []);

  // หรืออีกเบบคือการใช่use state โดยที่ไม่ต้องประกาศInterface โดยให้เราเปลี่ยน IMenu เป็นtype anyก็จะไม่ต้องประกาศ Interface
  // const [menu,setMenu] = useState<any[]>([]);
  return (
    <div className={classes.grid}>
      {menu.map((e) => (
        <Link href={`/page/menu/${e._id}`} key={e._id}>
          <div className={classes.cardImg}> 
            <img
              className={classes.img}
              src={e.img}
              alt={e.name}
            />  
          </div>
          <div className={classes.cardText}>
            <span className={classes.txt}>{e.name}</span>
          </div>
        </Link>
      ))}
    </div>
  );
}
